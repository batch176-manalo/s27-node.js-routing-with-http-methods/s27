/*
let http = require("http");

http.createServer(function(req,res){

	if(req.url === "/"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("Welcome to our new server!");
	} 

}).listen(4000);

console.log("Server is running at localhost:4000");
*/


/*
let http = require("http");

http.createServer(function(req,res){

	// HTTP Methods allow us to identify what action to take for our request.
	//With an HTTP Method we can actually routes that caters to the same endpoint but with a different action.
	// Most HTTP Methods are primarily concerned with CRUD operations:
	// Common HTTP Methods:
	// GET = GET method request indicates that the client wants to retrieve or GET data
	// POST = POST method request indicates that the client wants to send data to create a resource.
	// PUT = PUT method request indicates that the client wants to send data to update a resource.
	// DELETE = DELETE method request indicates that the client wants to delete a resource.

	console.log(req.url); //the request URL endpoint
	console.log(req.method); //the method of the request
	if(req.url === "/"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("Welcome to our new server!");
	} else if(req.url === "/"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a new route!");
	}

}).listen(4000);

console.log("Server is running at localhost:4000");
*/

/*
let http = require("http");

http.createServer(function(req,res){

	// HTTP Methods allow us to identify what action to take for our request.
	//With an HTTP Method we can actually routes that caters to the same endpoint but with a different action.
	// Most HTTP Methods are primarily concerned with CRUD operations:
	// Common HTTP Methods:
	// GET = GET method request indicates that the client wants to retrieve or GET data
	// POST = POST method request indicates that the client wants to send data to create a resource.
	// PUT = PUT method request indicates that the client wants to send data to update a resource.
	// DELETE = DELETE method request indicates that the client wants to delete a resource.

	console.log(req.url); //the request URL endpoint
	console.log(req.method); //the method of the request
	if(req.url === "/" && req.method === "GET"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a GET Method request");
	} else if(req.url === "/" && req.method === "POST"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a POST method request");
	} else if(req.url === "/" && req.method === "PUT"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a PUT method request");
	} else if(req.url === "/" && req.method === "DELETE"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a DELETE method request");
	}

}).listen(4000);

console.log("Server is running at localhost:4000");
*/

/*
let http = require("http");

let courses = [

	{
		name: "English 101",
		price: 23000,
		isActive: true
	},
	{
		name: "Math 101",
		price: 25000,
		isActive: true
	},
	{
		name:  "Reading 101",
		price: 21000,
		isActive: true
	}
];

http.createServer(function(req,res){

	console.log(req.url); 
	console.log(req.method); 

	if(req.url === "/" && req.method === "GET"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a GET Method request");
	} else if(req.url === "/" && req.method === "POST"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a POST method request");
	} else if(req.url === "/" && req.method === "PUT"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a PUT method request");
	} else if(req.url === "/" && req.method === "DELETE"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a DELETE method request");
	} else if(req.url === "/courses" && req.method === "GET"){

		// Since we are now passing a stringified JSON, so that the client which will receive our response be able to display our data properly, we have to update oour Content-Type headers to application/json
		res.writeHead(200,{'Content-Type':'application/json'});
		// We cannot pass another data type other than a string for our end().
		// So, to be bale to pass our array, we have to transform into a JSON string.
		res.end(JSON.stringify(courses));

	} else if(req.url === "/courses" && req.method === "POST"){


		// This route should be able to receive data from the client and we should be able to create a new course and add it to our courses array.


		// This will act as a placeholder to contain the data passed from the client.
		let requestBody = "";

		// Receiving data from a client to a nodejs server requires 2 steps:

		// data step - this part will read the stream of data coming from our client and process the incoming data into the requestBody variable.

		req.on('data',function(data){

			// console.log(data); //stream of data from client
			requestBody += data; //data stream is saved into the variable

		})


		// end step -this will run once or after the request data has been completely sent from our client.
		req.on('end',function(){

			console.log(requestBody);
			// requestBody now contains data from oour Postman Client
			// since requestBody is JSON format we have to parse to add it as an object in our array.
			requestBody = JSON.parse(requestBody);

			// Simulate creating document and adding it in a collection:
			let newCourse = {

				//requestBody is now an object, we will get the name property of the requestBody as the value for the name of our newCourse object. Same with price.
				name: requestBody.name, 
				price: requestBody.price,
				isActive: true

			}

			console.log(newCourse);
			courses.push(newCourse);
			console.log(courses);

			res.writeHead(200,{'Content-Type':'application/json'});
			// send the updated courses array to the client as a response
			res.end(JSON.stringify(courses));

		})

		
	}

}).listen(4000);

console.log("Server is running at localhost:4000");
*/

let http = require("http");

let courses = [

	{
		name: "English 101",
		price: 23000,
		isActive: true
	},
	{
		name: "Math 101",
		price: 25000,
		isActive: true
	},
	{
		name:  "Reading 101",
		price: 21000,
		isActive: true
	}
];

http.createServer(function(req,res){

	console.log(req.url); 
	console.log(req.method); 

	if(req.url === "/" && req.method === "GET"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a GET Method request");
	} else if(req.url === "/" && req.method === "POST"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a POST method request");
	} else if(req.url === "/" && req.method === "PUT"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a PUT method request");
	} else if(req.url === "/" && req.method === "DELETE"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a DELETE method request");
	} else if(req.url === "/courses" && req.method === "GET"){

		
		res.writeHead(200,{'Content-Type':'application/json'});
		res.end(JSON.stringify(courses));

	} else if(req.url === "/courses" && req.method === "POST"){

		let requestBody = "";

		

		req.on('data',function(data){

			
			requestBody += data; 

		})


		
		req.on('end',function(){

			console.log(requestBody);
			
			requestBody = JSON.parse(requestBody);

			
			let newCourse = {

				
				name: requestBody.name, 
				price: requestBody.price,
				isActive: true

			}

			console.log(newCourse);
			courses.push(newCourse);
			console.log(courses);

			res.writeHead(200,{'Content-Type':'application/json'});
			
			res.end(JSON.stringify(courses));

		})

		
	}

}).listen(4000);

console.log("Server is running at localhost:4000");